package functional;

import java.util.Map;
import java.util.HashMap;

import org.junit.*;
import org.junit.Before;

import play.db.jpa.JPAPlugin;
import play.Logger;
import play.test.*;

import play.mvc.*;
import play.mvc.Http.*;

import models.oauth.ClientInfo;
import models.oauth.User;

public class ApplicationTest extends FunctionalTest {

  @Before
  public void setUp() {
    Fixtures.deleteDatabase();
  }

  @Test
  public void testRegister() {
    // User "john" shouldn't exist in database
    assertTrue(!User.userExists("john"));

    // Insert the user "john" by sending the register request
    String expectedUsername = "john";
    String expectedPassword = "john";
    Map<String, String> params = new HashMap<String, String>();
    params.put("username", expectedUsername);
    params.put("password", expectedPassword);
    Response response = POST("/register", params);
    assertStatus(200, response);
    
    // Make sure the transaction is done
    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    // Find the user "john" in the database
    User user = User.findUser(expectedUsername);
    assertNotNull(user);
    assertEquals(expectedUsername, user.username);
    assertEquals(expectedPassword, user.password);
  }

  @Test
  public void testLoginFail() {
    User user = createUser("john", "john");

    Response response = loginUser("john", "mary", "/login");
    assertStatus(403, response);
  }

  @Test
  public void testLoginSuccess() {
    User user = createUser("john", "john");

    Response response = loginUser("john", "john", "/login");
    assertStatus(200, response);
  }

  @Test
  public void testApplyClientInfo() {
    User user = createUser("john", "john");
    String expectedClientType = "auth_code";
    String expectedRedirectURI = "http://www.example.com/callback";

    Response response = loginUser("john", "john", "/login");
    assertStatus(200, response);

    Map<String, String> params = new HashMap<String, String>();
    params.put("clientType", expectedClientType);
    params.put("redirectURI", expectedRedirectURI);
    response = POST("/apply", params);

    // Make sure the transaction is done
    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    // Find the clientInfo
    user = User.findUser("john");
    assertEquals(1, user.clientInfos.size());
    ClientInfo clientInfo = user.clientInfos.get(0);
    assertNotNull(clientInfo);
    assertEquals(expectedClientType, clientInfo.clientType);
    assertEquals(expectedRedirectURI, clientInfo.redirectURI);
    assertEquals(20, clientInfo.clientId.length());
    assertEquals(72, clientInfo.clientSecret.length());
  }

  // Utilities for testing
  private User createUser(String username, String password) {
    // Make sure the transaction is done
    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    User user = new User(username, password);
    user.save();

    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    return user;
  }

  private Response loginUser(String username, String password, String url) {
    Map<String, String>params = new HashMap<String, String>();
    params.put("username", username);
    params.put("password", password);
    return POST(url, params);
  }

}