package functional;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.HashMap;

import org.junit.*;
import org.junit.Before;

import play.db.jpa.JPAPlugin;
import play.Logger;
import play.test.*;

import play.mvc.*;
import play.mvc.Http.*;

import models.oauth.Authority;
import models.oauth.ClientInfo;
import models.oauth.User;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class OAuthServerTest extends FunctionalTest {

  @Before
  public void setUp() {
    Fixtures.deleteDatabase();
  }

  @Test
  public void testGrantTokenWrongClientId() {
    User user = createUser("john", "john");
    ClientInfo clientInfo = createClientInfo(user, "password", "http://www.example.com/callback");

    Map<String, String> params = new HashMap<String, String>();
    params.put("grant_type", "password");
    params.put("client_id", "does_not_exist");
    params.put("client_secret", "does_not_exist");
    Response response = GET("/oauth2/token" + generateQuery(params));
    // Extract JSON from response
    Map<String, String> json = new Gson().fromJson(
      response.out.toString(),
      new TypeToken<Map<String, String>>(){}.getType()
    );
    assertTrue(json.containsKey("error"));
    assertTrue(json.containsKey("error_description"));
  }

  @Test
  public void testGrantTokenWrongClientSecret() {
    User user = createUser("john", "john");
    ClientInfo clientInfo = createClientInfo(user, "password", "http://www.example.com/callback");

    Map<String, String> params = new HashMap<String, String>();
    params.put("grant_type", "password");
    params.put("client_id", clientInfo.clientId);
    params.put("client_secret", "does_not_exist");
    Response response = GET("/oauth2/token" + generateQuery(params));
    // Extract JSON from response
    Map<String, String> json = new Gson().fromJson(
      response.out.toString(),
      new TypeToken<Map<String, String>>(){}.getType()
    );
    assertTrue(json.containsKey("error"));
    assertTrue(json.containsKey("error_description"));
  }

  @Test
  public void testGrantTokenWrongUsername() {
    User user = createUser("john", "john");
    ClientInfo clientInfo = createClientInfo(user, "password", "http://www.example.com/callback");

    Map<String, String> params = new HashMap<String, String>();
    params.put("grant_type", "password");
    params.put("client_id", clientInfo.clientId);
    params.put("client_secret", clientInfo.clientSecret);
    params.put("username", "mary");
    params.put("username", "mary");
    Response response = GET("/oauth2/token" + generateQuery(params));
    // Extract JSON from response
    Map<String, String> json = new Gson().fromJson(
      response.out.toString(),
      new TypeToken<Map<String, String>>(){}.getType()
    );
    assertTrue(json.containsKey("error"));
    assertTrue(json.containsKey("error_description"));
  }

  @Test
  public void testGrantTokenWrongPassword() {
    User user = createUser("john", "john");
    ClientInfo clientInfo = createClientInfo(user, "password", "http://www.example.com/callback");

    Map<String, String> params = new HashMap<String, String>();
    params.put("grant_type", "password");
    params.put("client_id", clientInfo.clientId);
    params.put("client_secret", clientInfo.clientSecret);
    params.put("username", "john");
    params.put("username", "mary");
    Response response = GET("/oauth2/token" + generateQuery(params));
    // Extract JSON from response
    Map<String, String> json = new Gson().fromJson(
      response.out.toString(),
      new TypeToken<Map<String, String>>(){}.getType()
    );
    assertTrue(json.containsKey("error"));
    assertTrue(json.containsKey("error_description"));
  }

  @Test
  public void testGrantTokenSuccess() {
    User user = createUser("john", "john");
    ClientInfo clientInfo = createClientInfo(user, "password", "http://www.example.com/callback");

    Map<String, String> params = new HashMap<String, String>();
    params.put("grant_type", "password");
    params.put("client_id", clientInfo.clientId);
    params.put("client_secret", clientInfo.clientSecret);
    params.put("username", "john");
    params.put("password", "john");
    Response response = GET("/oauth2/token" + generateQuery(params));
    // Extract JSON from response
    Map<String, String> json = new Gson().fromJson(
      response.out.toString(),
      new TypeToken<Map<String, String>>(){}.getType()
    );
    assertTrue(json.containsKey("access_token"));
    assertTrue(json.containsKey("expires_in"));
    assertTrue(json.containsKey("refresh_token"));
    assertEquals(72, json.get("access_token").length());
    assertEquals("3600", json.get("expires_in"));
    assertEquals(72, json.get("refresh_token").length());
  }

  @Test
  public void testRefeshTokenSuccess() {
    User user = createUser("john", "john");
    ClientInfo clientInfo = createClientInfo(user, "password", "http://www.example.com/callback");

    Map<String, String> params = new HashMap<String, String>();
    params.put("grant_type", "password");
    params.put("client_id", clientInfo.clientId);
    params.put("client_secret", clientInfo.clientSecret);
    params.put("username", "john");
    params.put("password", "john");
    Response response = GET("/oauth2/token" + generateQuery(params));
    // Extract JSON from response
    Map<String, String> json = new Gson().fromJson(
      response.out.toString(),
      new TypeToken<Map<String, String>>(){}.getType()
    );

    String accessToken = json.get("access_token");
    String refreshToken = json.get("refresh_token");


    // Generate refresh token request
    params = new HashMap<String, String>();
    params.put("grant_type", "refresh_token");
    params.put("client_id", clientInfo.clientId);
    params.put("client_secret", clientInfo.clientSecret);
    params.put("refresh_token", refreshToken);
    response = POST("/oauth2/token", params);
    json = new Gson().fromJson(
      response.out.toString(),
      new TypeToken<Map<String, String>>(){}.getType()
    );
    assertTrue(json.containsKey("access_token"));
    assertTrue(json.containsKey("expires_in"));
    assertTrue(json.containsKey("refresh_token"));
    assertEquals(72, json.get("access_token").length());
    assertEquals("3600", json.get("expires_in"));
    assertEquals(72, json.get("refresh_token").length());
    assertFalse(accessToken.equals(json.get("access_token")));
    assertFalse(refreshToken.equals(json.get("refresh_token")));
  }

  // Utilities for testing
  private User createUser(String username, String password) {
    // Make sure the transaction is done
    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    User user = new User(username, password);
    user.save();

    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    return user;
  }

  private ClientInfo createClientInfo(User user, String clientType, String redirectURI) {
    // Make sure the transaction is done
    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    ClientInfo clientInfo = new ClientInfo(user, clientType, redirectURI);
    clientInfo.save();

    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    return clientInfo;
  }

  private String generateQuery(Map<String, String> map) {
    String query = "";
    if(map.size() > 0) {
      query += "?";
      for(String key : map.keySet()) {
        try {
          query += URLEncoder.encode(key, "UTF-8");
          query += "=";
          query += URLEncoder.encode(map.get(key), "UTF-8");
          query += "&";
        } catch(UnsupportedEncodingException e) {}
      }
    }
    return query.substring(0, query.length() - 1);
  }

}