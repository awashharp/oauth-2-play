package functional;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.HashMap;

import org.junit.*;
import org.junit.Before;

import play.db.jpa.JPAPlugin;
import play.Logger;
import play.test.*;
import play.mvc.*;
import play.mvc.Http.*;

import models.oauth.Authority;
import models.oauth.ClientInfo;
import models.oauth.User;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UserActionTest extends FunctionalTest {

  @Before
  public void setUp() {
    Fixtures.deleteDatabase();
  }

  @Test
  public void testGetUserIdWorngAccessToken() {
    User user = createUser("john", "john");
    ClientInfo clientInfo = createClientInfo(user, "password", "http://www.example.com/callback");
    String accessToken = generateAccessToken(clientInfo, user);

    Map<String, String> params = new HashMap<String, String>();
    params.put("access_token", "does_not_exist");
    Response response = POST("/user/id", params);
    // Extract JSON from response
    Map<String, String> json = new Gson().fromJson(
      response.out.toString(),
      new TypeToken<Map<String, String>>(){}.getType()
    );
    assertTrue(json.containsKey("error"));
    assertTrue(json.containsKey("error_description"));
  }

  @Test
  public void testGetUserIdSuccess() {
    User user = createUser("john", "john");
    ClientInfo clientInfo = createClientInfo(user, "password", "http://www.example.com/callback");
    String accessToken = generateAccessToken(clientInfo, user);

    Map<String, String> params = new HashMap<String, String>();
    params.put("access_token", accessToken);
    Response response = POST("/user/id", params);
    // Extract JSON from response
    Map<String, String> json = new Gson().fromJson(
      response.out.toString(),
      new TypeToken<Map<String, String>>(){}.getType()
    );
    assertTrue(json.containsKey("username"));
    assertTrue(json.containsKey("userid"));
    assertEquals(user.username, json.get("username"));
    assertEquals(user.id.toString(), json.get("userid"));
  }

  // Utilities for testing
  private User createUser(String username, String password) {
    // Make sure the transaction is done
    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    User user = new User(username, password);
    user.save();

    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    return user;
  }

  private ClientInfo createClientInfo(User user, String clientType, String redirectURI) {
    // Make sure the transaction is done
    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    ClientInfo clientInfo = new ClientInfo(user, clientType, redirectURI);
    clientInfo.save();

    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    return clientInfo;
  }

  private String generateAccessToken(ClientInfo clientInfo, User target) {
    // Make sure the transaction is done
    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    Authority authority = new Authority(clientInfo, Authority.FlowType.PASSWORD, target).save();
    authority.grantToken(true);

    JPAPlugin.closeTx(false);
    JPAPlugin.startTx(false);

    return authority.accessToken;
  }

  private String generateQuery(Map<String, String> map) {
    String query = "";
    if(map.size() > 0) {
      query += "?";
      for(String key : map.keySet()) {
        try {
          query += URLEncoder.encode(key, "UTF-8");
          query += "=";
          query += URLEncoder.encode(map.get(key), "UTF-8");
          query += "&";
        } catch(UnsupportedEncodingException e) {}
      }
    }
    return query.substring(0, query.length() - 1);
  }
}