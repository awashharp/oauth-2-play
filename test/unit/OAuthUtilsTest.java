package unit;

import org.junit.*;
import org.junit.Before;

import java.util.regex.Pattern;

import play.Logger;
import play.test.*;

import utils.OAuthUtils;

public class OAuthUtilsTest extends UnitTest {

  @Test
  public void testRandomString() {
    int expectedLength = 24;
    String token = OAuthUtils.randomString(expectedLength);
    assertEquals(expectedLength, token.length());
    assertTrue(Pattern.matches("[a-zA-Z_0-9-]+", token));
  }
}