package unit;

import org.junit.*;
import org.junit.Before;

import java.util.*;

import play.test.*;

import models.oauth.User;

public class UserTest extends UnitTest {

  @Before
  public void setUp() {
    Fixtures.deleteDatabase();

    // Database data
    User user = new User("john", "john");
    user.save();
  }

  @Test
  public void testUserExists() {
    assertTrue(User.userExists("john"));
    assertTrue(!User.userExists("mary"));
  }

  @Test
  public void testFindUser() {
    User user = User.findUser("john");
    assertNotNull(user);
    assertEquals("john", user.username);
    assertEquals("john", user.password);
    assertNull(User.findUser("mary"));
  }

}
