package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.oauth.ClientInfo;
import models.oauth.User;

@With(LoginCheck.class)
public class Application extends Controller {

  public static void index() {
    render();
  }

  public static void register(String username, String password) {
    boolean result = false;
    if(User.userExists(username)) {
      error();
    }
    result = new User(username, password).create();
    if(result == false) {
      error();
    }
    ok();
  }

  public static void applyClientInfo(String clientType, String redirectURI) {
    boolean result = false;
    User owner = renderArgs.get("user", User.class);
    if(owner == null) {
      forbidden();
    }
    result = new ClientInfo(owner, clientType, redirectURI).create();
    if(result == false) {
      error();
    } else {
      ok(); 
    }
  }

}