package controllers;

import play.*;
import play.mvc.*;

import java.util.HashMap;
import java.util.Map;

import models.oauth.User;

@With(OAuthCheck.class)
public class UserAction extends Controller {

  public static void getUserId() {
    User user = (User)renderArgs.get("user");
    Map<String, String> result = new HashMap<String, String>();
    result.put("username", user.username);
    result.put("userid", user.id.toString());
    renderJSON(result);
  }
}