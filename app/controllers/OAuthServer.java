package controllers;

import play.*;
import play.mvc.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import models.oauth.Authority;
import models.oauth.Authority.FlowType;
import models.oauth.ClientInfo;
import models.oauth.User;

public class OAuthServer extends Controller {

  public static void token(
      String grant_type,
      String client_id,
      String client_secret) {

    Map<String, String> result = new HashMap<String, String>();

    // Username/Password Grant
    if("password".equals(grant_type)) {
      // Check client's credential
      ClientInfo clientInfo = ClientInfo.findClientInfo(client_id);
      if(clientInfo == null) {
        result.put("error", "invalid_request");
        result.put("error_description", "Unknown client id.");
        renderJSON(result);
      }
      if(!clientInfo.clientSecret.equals(client_secret)) {
        result.put("error", "access_denied");
        result.put("error_description", "Client credential is invalid.");
        renderJSON(result);
      }

      // Check user credentials
      String username = params.get("username");
      String password = params.get("password");
      if(username == null || password == null) {
        result.put("error", "invalid_request");
        result.put("error_description", "Missing username or password.");
        renderJSON(result);
      }
      User target = User.findUser(username);
      if(target == null || !target.password.equals(password)) {
        result.put("error", "access_denied");
        result.put("error_description", "Wrong username or password.");
      }

      // Grant the access
      Authority authority = new Authority(clientInfo, FlowType.PASSWORD, target);
      if(!authority.grantToken(true)) {
        result.put("error", "invalid_request");
        result.put("error_description", "Access token granting failed.");
        renderJSON(result);
      }
      authority.save();
      // Return access token
      result.put("access_token", authority.accessToken);
      result.put("expires_in", String.valueOf(3600));
      result.put("refresh_token", authority.refreshToken);
      renderJSON(result);
    }
    // Refresh Token
    else if("refresh_token".equals(grant_type)) {
      // Check client's credential
      ClientInfo clientInfo = ClientInfo.findClientInfo(client_id);
      if(clientInfo == null) {
        result.put("error", "invalid_request");
        result.put("error_description", "Unknown client id.");
        renderJSON(result);
      }
      if(!clientInfo.clientSecret.equals(client_secret)) {
        result.put("error", "access_denied");
        result.put("error_description", "Client credential is invalid.");
        renderJSON(result);
      }

      // Check refresh token's validity
      String refreshToken = params.get("refresh_token");
      Authority authority = Authority.findByRefreshToken(refreshToken);
      if(authority == null) {
        result.put("error", "invalid_request");
        result.put("error_description", "Refresh token is invalid.");
        renderJSON(result);
      }
      if(authority.refreshTokenExpireTimestamp.before(new Date())) {
        result.put("error", "invalid_request");
        result.put("error_description", "Refresh token is expired.");
        renderJSON(result);
      }
      // Let the old authority expire
      authority.expireToken();
      // Give another access token
      Authority newAuthority = new Authority(authority.clientInfo, FlowType.PASSWORD, authority.target);
      if(!newAuthority.grantToken(true)) {
        result.put("error", "invalid_request");
        result.put("error_description", "Access token granting failed.");
        renderJSON(result);
      }
      newAuthority.save();
      // Return access token
      result.put("access_token", newAuthority.accessToken);
      result.put("expires_in", String.valueOf(3600));
      result.put("refresh_token", newAuthority.refreshToken);
      renderJSON(result);
    }
    else {
      error();
    }
  }

}