package controllers;

import play.*;
import play.mvc.*;

import models.oauth.User;

/**
 * Handling user login imformation
 */

public class LoginCheck extends Controller {

  public static void login(String username, String password) {
    if(username == null || password == null) {
      error();
    }
    User user = User.findUser(username);
    if(!user.password.equals(password)) {
      forbidden();
    }
    else {
      // Let user login using this data
      session.put("username", username);
      ok();
    }
  }

  @Before(unless = {"login", "Application.index", "Application.register"})
  public static void checkLogin() {
    String username = session.get("username");
    if(username == null || username.equals("")) {
      forbidden();
    }
  }

  @Before(unless = {"login", "Application.index", "Application.register"})
  public static void insertUser() {
    String username = session.get("username");
    if(username != null && !username.equals("")) {
      renderArgs.put("user", User.findUser(username));
    }
  }
}