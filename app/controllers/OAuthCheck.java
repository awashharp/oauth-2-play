package controllers;

import play.*;
import play.mvc.*;

import java.util.HashMap;
import java.util.Map;

import models.oauth.User;
import models.oauth.Authority;

public class OAuthCheck extends Controller {

  @Before
  public static void checkAccessToken() {
    Map<String, String> result = new HashMap<String, String>();
    String accessToken = params.get("access_token");
    if("".equals(accessToken)) {
      result.put("error", "invalid_access_token1");
      result.put("error_description", "The access token is invalid.");
      renderJSON(result);
    }
    Authority authority = Authority.findByAccessToken(accessToken);
    if(authority == null) {
      result.put("error", "invalid_access_token2");
      result.put("error_description", "The access token is invalid.");
      renderJSON(result);
    }
    else if(authority.state == Authority.AuthState.EXPIRED) {
      result.put("error", "access_token_expired");
      result.put("error_description", "The access token is expired.");
      renderJSON(result);
    }
    // Add the target user into template scope
    renderArgs.put("user", authority.target);
  }
}