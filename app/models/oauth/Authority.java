package models.oauth;

import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.db.jpa.Model;

import utils.OAuthUtils;

/**
 * Authority of OAuth 2.0
 *
 * This is a generalized data for all kinds of flow.
 * It contains authorization code, access token, and refresh token fields for authorization.
 */
@Entity
@Table(name = "authority")
public class Authority extends Model {

  @ManyToOne(optional = false)
  @JoinColumn(name = "client_id")
  public ClientInfo clientInfo;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  public AuthState state;

  @Column(name = "flow_type", nullable = false)
  @Enumerated(EnumType.STRING)
  public FlowType flowType;

  @ManyToOne
  @JoinColumn(name = "target_user_id")
  public User target;

  @Column(name = "auth_code")
  public String authCode;

  @Column(name = "code_create_ts")
  @Temporal(TemporalType.TIMESTAMP)
  public Date codeCreateTimestamp;

  @Column(name = "code_auth_ts")
  @Temporal(TemporalType.TIMESTAMP)
  public Date codeAuthTimestamp;

  @Column(name = "access_token")
  public String accessToken;

  @Column(name = "at_expire_ts")
  @Temporal(TemporalType.TIMESTAMP)
  public Date accessTokenExpireTimestamp;

  @Column(name = "refresh_token")
  public String refreshToken;

  @Column(name = "rt_expire_ts")
  @Temporal(TemporalType.TIMESTAMP)
  public Date refreshTokenExpireTimestamp;


  /* Default constructor, not issuing any token or code */
  public Authority(ClientInfo clientInfo, FlowType flowType, User target) {
    this.clientInfo = clientInfo;
    this.flowType = flowType;
    this.state = AuthState.CREATED;
    this.target = target;
  }

  public boolean grantToken(boolean withRefreshToken) {
    // AccessToken in this authority is issued
    if(state != AuthState.CREATED) {
      // This function should only be called once
      state = AuthState.REFUSED;
      return false;
    }

    // Issue the access token
    accessToken = OAuthUtils.randomString(72);
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.SECOND, 3600);
    accessTokenExpireTimestamp = calendar.getTime();
    if(withRefreshToken) {
      refreshToken = OAuthUtils.randomString(72);
      calendar = Calendar.getInstance();
      calendar.add(Calendar.SECOND, 7200);
      refreshTokenExpireTimestamp = calendar.getTime();
    }
    this.state = AuthState.ISSUED;
    this.save();
    return true;
  }

  public void expireToken() {
    this.state = AuthState.EXPIRED;
    this.save();
  }

  public static Authority findByAccessToken(String accessToken) {
    Authority authority = Authority.find("byAccessToken", accessToken).first();
    if(authority != null && authority.accessTokenExpireTimestamp.before(new Date())) {
      authority.expireToken();
      authority = null;
    }
    return authority;
  }

  public static Authority findByRefreshToken(String refreshToken) {
    return Authority.find("byRefreshToken", refreshToken).first();
  }


  /**
   * Flow Type Client is using
   *
   * SERVER: The server-side authorization grant type flow
   * CLIENT: The implicit grant type flow
   * PASSWORDL The username-password grant type flow
   */
  public static enum FlowType {
    NONE("NONE"), SERVER("SERVER"), CLIENT("CLIENT"), PASSWORD("PASSWORD");

    private String name;

    private FlowType(String name) {
      this.name = name;
    }

    @Override
    public String toString() {
      return name;
    }
  }

  /** 
   * State of Authority
   *
   * CREATED: When authority is create, but not yet create access token
   * EXPIRED: When access token is expired, but not refresh token
   * ISSUED:  When access token is issued and it's not expired
   * REFUSED: When part of the authorization process is illegal,
   *          and this authority is can't be used anymore
   */
  public static enum AuthState {
    NONE("NONE"), CREATED("CREATED"), EXPIRED("EXPIRED"), ISSUED("ISSUED"), REFUSED("REFUSED");

    private String name;

    private AuthState(String name) {
      this.name = name;
    }

    @Override
    public String toString() {
      return name;
    }
  }

}
