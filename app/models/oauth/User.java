package models.oauth;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.jpa.Model;

/**
 * OAuth 2.0 Resource Owner
 */

@Entity
@Table(name = "user")
public class User extends Model {

  @Column(nullable = false)
  public String username;
  @Column(nullable = false)
  public String password;

  @OneToMany(cascade=CascadeType.ALL, mappedBy="owner")
  public List<ClientInfo> clientInfos;

  @OneToMany(cascade=CascadeType.ALL, mappedBy="target")
  public List<Authority> authorities;


  public User(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public static boolean userExists(String username) {
    List<User> users = User.find("byUsername", username).fetch();
    return users != null && users.size() > 0;
  }

  public static User findUser(String username) {
    return User.find("byUsername", username).first();
  }

}