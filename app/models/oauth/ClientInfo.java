package models.oauth;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.jpa.Model;

import utils.OAuthUtils;

/**
 * Client Registry Table
 *
 * Storing the infomation about client, and each data is owned by a client
 */

@Entity
@Table(name = "client_info")
public class ClientInfo extends Model {

  @ManyToOne(optional = false)
  @JoinColumn(name="owner_id")
  public User owner;

  @Column(name = "client_id", nullable = false)
  public String clientId;

  @Column(name="client_secret", nullable = false)
  public String clientSecret;

  @Column(name="client_type", nullable = false)
  public String clientType;

  @Column(name="redirect_uri")
  public String redirectURI;

  @OneToMany(cascade=CascadeType.ALL, mappedBy="clientInfo")
  public List<Authority> authorities;


  public ClientInfo(User owner, String clientType, String redirectURI) {
    this.owner = owner;
    this.clientId = OAuthUtils.randomString(20);
    this.clientSecret = OAuthUtils.randomString(72);
    this.clientType = clientType;
    this.redirectURI = redirectURI;
  }

  public static ClientInfo findClientInfo(String clientId) {
    return ClientInfo.find("byClientId", clientId).first();
  }

}