package utils;

import java.security.SecureRandom;

import org.apache.commons.codec.binary.Base64;

public class OAuthUtils {

  public static String randomString(int length) {
    SecureRandom random = new SecureRandom();
    byte bytes[] = new byte[length];
    random.nextBytes(bytes);

    byte base64[] = Base64.encodeBase64(bytes, false, true);

    return new String(base64).substring(0, length);
  }
}