# OAuth 2.0 Simple Framework for Play! Framework

This is a simple [OAuth 2.0](http://tools.ietf.org/html/rfc6749) implementation on Play! Framework. This implementation is not intend for serious use, because this only implement partial OAuth 2.0 Specification.

# Documentation

(Leaving empty...)